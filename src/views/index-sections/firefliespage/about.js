import React from "react";

// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components

function About() {
  return (
    <>
      <div
        className="section section-about"
        data-background-color="black"
        id="about-section"
      >
        <Container>
          <Row className="justify-content-md-center">
            <Col className="text-center" lg="8" md="12">
              <br/>
              <h3 className="title">About</h3>
              <p>
                This website is all story about firefly. 
                Netizen could see history and hidden fact about firefly. Firefly is categorize by the lampyridae. 
                The Lampyridae are a family of insects in the beetle order Coleoptera with over 2,000 described species.
                They are soft-bodied beetles that are commonly called fireflies or lightning bugs for their conspicuous use of bioluminescence during twilight to attract mates or prey. Fireflies produce a "cold light", with no infrared or ultraviolet frequencies.
                This chemically produced light from the lower abdomen may be yellow, green, or pale red, with wavelengths from 510 to 670 nanometers.
                Some species such as the dimly glowing "blue ghost" of the Eastern US are commonly thought to emit blue light (490 nanometers), though this is a false perception of their truly green emission light due to the Purkinje effect. 
              </p>
              <p>
                Fireflies are found in temperate and tropical climates.
                Many are found in marshes or in wet, wooded areas where their larvae have abundant sources of food. 
                Some species are called "glowworms" in Eurasia and elsewhere. 
                While all known fireflies glow, only some adults produce light and the location of the light organ varies among species and between sexes of the same species. The form of the insect which emits light varies from species to species (for example, in the glow worm found in the UK, Lampyris noctiluca, it is the female that is most easily noticed.).
                In the Americas, "glow worm" also refers to the closely related family Phengodidae.
                In New Zealand and Australia the term "glow worm" is in use for the luminescent larvae of the fungus gnat Arachnocampa.
                In many species of fireflies, both male and female fireflies have the ability to fly, but in some species, the females are flightless.
                for further information, you can surfing through our website.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default About;
