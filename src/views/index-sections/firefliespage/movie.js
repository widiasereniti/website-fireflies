import React from "react";

// reactstrap components
import { Container, Row, Col } from "reactstrap";

// core components

function Movies() {
  return (
    <>
      <div className="section section-movies" id="movies">
        <Container>
          <Row>
            <Col md="12">
              <div className="hero-Movies-container">
                <img
                  alt="..."
                  src={require("assets/img/hero-image-1.png")}
                ></img>
              </div>
              <div className="hero-Movies-container-1">
                <img
                  alt="..."
                  src={require("assets/img/hero-image-2.png")}
                ></img>
              </div>
              <div className="hero-Movies-container-2">
                <img
                  alt="..."
                  src={require("assets/img/hero-image-3.png")}
                ></img>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default Movies;
