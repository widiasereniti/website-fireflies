import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Container,
  Row,
  Col
} from "reactstrap";

// core components

function Tabs() {
  const [iconPills, setIconPills] = React.useState("1");
  const [pills, setPills] = React.useState("1");
  return (
    <>
      <div className="section section-tabs" id="tabs">
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" md="10" xl="12">
              <p className="category">Fireflies Content</p>
              <Card>
                <CardHeader>
                  <Nav className="justify-content-center" role="tablist" tabs>
                    <NavItem>
                      <NavLink
                        className={iconPills === "1" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("1");
                        }}
                      >
                        {/* <i className="now-ui-icons objects_umbrella-13"></i> */}
                        Diet Information
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "2" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("2");
                        }}
                      >
                        {/* <i className="now-ui-icons shopping_cart-simple"></i> */}
                        Mating & Reproduction
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "3" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("3");
                        }}
                      >
                        {/* <i className="now-ui-icons shopping_shop"></i> */}
                        Animal Habitat
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "4" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("4");
                        }}
                      >
                        {/* <i className="now-ui-icons ui-2_settings-90"></i> */}
                        Animal Anatomy
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "5" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("5");
                        }}
                      >
                        {/* <i className="now-ui-icons ui-2_settings-90"></i> */}
                        Scientific
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "6" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("6");
                        }}
                      >
                        {/* <i className="now-ui-icons ui-2_settings-90"></i> */}
                        Behaviour
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "7" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("7");
                        }}
                      >
                        {/* <i className="now-ui-icons ui-2_settings-90"></i> */}
                        Communication
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "8" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("8");
                        }}
                      >
                        {/* <i className="now-ui-icons ui-2_settings-90"></i> */}
                        Miscellanous Interesting
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        className={iconPills === "9" ? "active" : ""}
                        href="#pablo"
                        onClick={e => {
                          e.preventDefault();
                          setIconPills("9");
                        }}
                      >
                        {/* <i className="now-ui-icons ui-2_settings-90"></i> */}
                        Popular Culture
                      </NavLink>
                    </NavItem>
                  </Nav>
                </CardHeader>


                <CardBody>
                  <TabContent
                    className="text-center"
                    activeTab={"iconPills" + iconPills}
                  >
                    <TabPane tabId="iconPills1">
                      <h5>Diet Information</h5>
                      <p>
                        Feeding fireflies are plant juices, small snails, worms and other insects.
                        If you want to keep fireflies, you should prepare a place that is designed almost the same as its natural habitat. It doesn't have to be as broad as the original. The important thing is the fireflies are comfortable and comfortable staying there. Because if fireflies don't feel comfortable, they don't want to live there. The food can be taken from nature around us
                      </p>
                    </TabPane>
                    <TabPane tabId="iconPills2">
                        <h5>Mating & Reproduction</h5>
                        <img
                          alt="..."
                          src={require("assets/img/Mating.png")}
                        ></img>
                    </TabPane>
                    <TabPane tabId="iconPills3">
                      <h5>Animal Habitat</h5>
                      <p>
                      Fireflies live in various habitats. Many species thrive in forests, fields or the margins between them. Some live in more arid areas, but they typically follow the rainy season. 
                      Fireflies are found all over the world, from North and South America to Europe and Asia.
Most firefly species have one thing in common: standing water. 
They live near ponds, streams, marshes, rivers and lakes, but they don’t need a lot of water to get by. 
Vernal pools and small depressions that hold water during firefly mating season can all provide the habitat fireflies need. 
Most firefly species live at the margins where forest or field meet water.

Scientists aren’t completely sure what most species of fireflies eat. It’s probable that firefly larvae feed on different prey from that of adult fireflies. The larvae are believed to be carnivorous, living off smaller insects, snails and slugs. Adult fireflies may also live on other insects, as well as pollen and plants, but it’s possible that some species don’t eat anything—their lifespan is only a few weeks long. But scientists believe fireflies thrive in wet areas because their prey does as well—including other insects and insect larvae, slugs and snails.

Fireflies love humid, warm environments. In the U.S., almost no species of fireflies are found west of Kansas—although there are also warm and humid areas to the west. Nobody is sure why this is. There are many species of fireflies throughout the world, and the most diversity in species is found in tropical Asia as well as Central and South America.
                      </p>
                    </TabPane>
                    <TabPane tabId="iconPills4">
                      <h5>Animal Anatomy</h5>
                      <img
                        alt="..."
                        src={require("assets/img/Anatomy.png")}
                      ></img>
                    </TabPane>
                    <TabPane tabId="iconPills5">
                      <h5>Scientific Name and Classification</h5>
                      <p>
                      Scientifically, fireflies are classified under Lampyridae, a family of insects within the beetle order Coleoptera, or winged beetles. While most fireflies are characterized by their use of bioluminescence to attract mates and communicate with others in their species, not all insects within the firefly family produce light. Some communicate using pheromones, a sort of insect perfume.

There are thousands of firefly species spread across temperate and tropical zones all over the world. In New England alone, you might see twenty or thirty species. But all fireflies are classified under five main subfamilies.

Lampyridinae (subfamily)

Photinini

If you live in North America, these are the fireflies you’re probably most familiar with. Not all fireflies in this subfamily light up, but those that do are generally divided into many closely related species.

Photinus fireflies tend to be the most common of this group; about half an inch long, these produce yellow-green light. The most common amoung this group is Photinus pyralis, or the common eastern firefly. This is most likely what most people will see in their backyards at night. Below is a picture of Photinus pyralis, the most common firefly species you will see at night.
                      
                      </p>
                      <img
                        alt="..."
                        src={require("assets/img/Scientific.png")}
                      ></img>
                    </TabPane>
                    <TabPane tabId="iconPills6">
                      <h5>Scientific Name and Classification</h5>
                      <br/>
                      <h6>Flight</h6>
                      <p>
                      Like all other beetles, lightning bugs have hardened forewings called elytra, which meet in a straight line down the back when at rest. In-flight, fireflies hold the elytra out for balance, relying on their membranous hindwings for movement. These traits place fireflies squarely in the order Coleoptera.
                      </p>
                      <h6>Efficient Light Producers</h6>
                      <p>An incandescent light bulb gives off 90% of its energy as heat and only 10% as light, which you'd know if you've touched one that's been on for a while. If fireflies produced that much heat when they lit up, they would incinerate themselves. Fireflies produce light through an efficient chemical reaction called chemiluminescence that allows them to glow without wasting heat energy. For fireflies, 100% of the energy goes into making light; accomplishing that flashing increases the firefly metabolic rates an astonishingly low 37% above resting values.
                      </p>
                      <h6>Some Are Cannibals</h6>
                      <p>What adult fireflies eat is largely unknown. Most don't seem to feed at all, while others are believed to eat mites or pollen. We do know that Photuris fireflies eat other fireflies. Photuris females enjoy munching on males of other genera.
                      </p>
                    </TabPane>
                    <TabPane tabId="iconPills7">
                      <h5>How Firefly Communicate with Each Other</h5>
                      <p>Fireflies talk to each other with light.
                        Fireflies emit light mostly to attract mates, although they also communicate for other reasons as well, such as to defend territory and warn predators away. In some firefly species, only one sex lights up. In most, however, both sexes glow; often the male will fly, while females will wait in trees, shrubs and grasses to spot an attractive male. If she finds one, she’ll signal it with a flash of her own.
                      </p>
                    </TabPane>
                    <TabPane tabId="iconPills8">
                      <h5>Miscellaneous Interesting Facts</h5>
                      <p>Bla...</p>
                    </TabPane>
                    <TabPane tabId="iconPills9">
                      <h5>Popular Culture</h5>
                      <p>Bla...</p>
                    </TabPane>
                  </TabContent>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default Tabs;
